#!/usr/bin/env bash

## Data was downloaded from UCSC using the following call (similar call for the HIAB data)
# rsync -avzP rsync://hgdownload.cse.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeCaltechRnaSeq/*.fastq.tgz ./

usage() {
    NAME=$(basename $0)
    cat <<EOF
${NAME} <path to fastq files>
Renaming extracting ENCODE RNA-seq files
example runs:
./FetchENCODE.sh /scratchBulk/dob2014/SEQC/ENCODE/Caltech/H1-hESC
EOF

}

if [ $# -ne 1 ]
then
    usage
    exit 1
fi

FASTQLOC="$1"

cd ${FASTQLOC}

samples=(`ls ./*.fastq.tgz`)
for ((i=0; i<=${#samples[@]}-1; i++)); do
    sample=$(basename ${samples[$i]%.fastq.tgz})
    echo "${sample}"
    mkdir ${sample}
    tar xvzf ${FASTQLOC}/${sample}.fastq.tgz -C ${sample} --strip-components 1
    cat ${FASTQLOC}/${sample}/*.fastq > ${FASTQLOC}/${sample}.fastq
    gzip ${FASTQLOC}/${sample}.fastq
    rm -rf ${FASTQLOC}/${sample}/
    echo "Completed ${sample}"
done